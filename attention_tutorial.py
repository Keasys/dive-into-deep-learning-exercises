import torch
from torch import nn
from torch.nn import functional as F
import matplotlib.pyplot as plt

###############################################################################
#                                   heatmaps                                  #
###############################################################################

def a(q,k):
    return torch.dot(q,k.T)

def show_heatmaps(matrices, xlabel, ylabel, titles=None, figsize=(2.5,2.5), cmap="Reds"):
    '''Visualization of attention weights, the input matrices
    is of shape (#rows for display x #columns for display x #keys, #queries)'''
    
    num_rows, num_cols, _, _ =  matrices.shape
    fig, axes = plt.subplots(num_rows, num_cols, figsize=figsize, sharex=True, sharey=True, squeeze=False)
    
    for i, (row_axes, row_matrices) in enumerate(zip(axes, matrices)):
        for j, (ax, matrix) in enumerate(zip(row_axes, row_matrices)):
            
            pcm = plt.imshow(matrix.detach().numpy(), cmap=cmap)

            if i == num_rows - 1:
                ax.set_xlabel(xlabel)
                
            if j == 0:
                ax.set_ylabel(ylabel)

            if titles:
                ax.set_title(titles[j])

    fig.colorbar(pcm, ax=axes, shrink=0.6)
    plt.show()


attention_weights = torch.eye(10).reshape((1,1,10,10))
show_heatmaps(attention_weights, xlabel="Keys", ylabel="Queries")

###############################################################################
#                       attention pooling by similarity                       #
###############################################################################


# kernels definitions

def gaussian(x):
    return torch.exp(-x**2 / 2)
def boxcar(x):
    return torch.abs(x) < 1.0
def constant(x):
    return 1.0 + 0*x
def epanechikov(x):
    return torch.max(1-torch.abs(x), torch.zeros_like(x))


fig, axes = plt.subplots(1, 4, sharey=True, figsize=(12, 3))

kernels = (gaussian, boxcar, constant, epanechikov)
names = ('Gaussian', 'Boxcar', 'Constant', 'Epanechikov')

x = torch.arange(-2.5, 2.5, 0.1)

for kernel, name, ax in zip(kernels,names, axes):
    ax.plot(x.detach().numpy(), kernel(x).detach().numpy())
    ax.set_xlabel(name)

plt.show()


###############################################################################
#                           training data definition                          #
###############################################################################

def f(x):
    '''Let's define this function to see Nadaraya-Watson regression'''
    
    return 2 * torch.sin(x) + x


def nadaraya_watson(x_train, y_train, x_val, kernel):
    dists = x_train.reshape((-1,1)) - x_val.reshape((1,-1))

    # each column/row corresponds to each query/key
    k = kernel(dists).type(torch.float32)

    # normalization over keys for each query
    attention_weights = k / k.sum(0)

    # estimate y
    y_hat = y_train @ attention_weights

    return y_hat, attention_weights

def plot(x_train, y_train, y_val, kernels, names, attention=False):
    fig, axes = plt.subplots(1,4,sharey=True, figsize=(13,4))

    for kernel, name, ax in zip(kernels, names, axes):
        y_hat, attention_weights = nadaraya_watson(x_train, y_train, x_val, kernel)
        if attention:
            pcm = ax.imshow(attention_weights.detach().numpy(), cmap="Reds")
        else:
            ax.plot(x_val, y_hat)
            ax.plot(x_val, y_val, 'm--')
            ax.plot(x_train, y_train, 'o', alpha=0.5)
        ax.set_xlabel(name)
        if not attention:
            ax.legend(['y_hat', 'y'])
    if attention:
        fig.colorbar(pcm, ax=axes, shrink=0.7)
    plt.show()

# define the examples in the data
n = 40
x_train, _ = torch.sort(torch.rand(n) * 5)
y_train = f(x_train)+torch.randn(n)

# define the labels for the data
x_val = torch.arange(0,5,0.1)
y_val = f(x_val)

plot(x_train, y_train, y_val, kernels, names)
plot(x_train, y_train, y_val, kernels, names, attention=True)


###############################################################################
#                           non-parametric attention                          #
###############################################################################

class NWRegression(nn.Module):
    """Model for optimization of attention weights
    

    """
    def __init__(self, **kwargs):
        super(NWRegression, self).__init__(**kwargs)
        self.loss = torch.nn.MSELoss()
        
